#include "VM.h"


void VM::copy_code(std::ifstream program) {
    if (program.is_open()) {
        // Seek to the end of the file
        program.seekg(program.end);
        // Get the size of the file
        int length = program.tellg();
        this->code = new char[length];
        program.read(this->code, length);
    }
}


VM::VM(std::ifstream program) {
    this->memory.reserve(30000);
    this->pc = 0;
    //this->copy_code(program);
}


VM::VM(char *code): code(code), pc(0) {
    this->memory.reserve(30000);
}


void VM::execute() {
}
