#include <vector>
#include <iostream>
#include <fstream>


class VM {
    private:
        std::vector<int> memory;
        int pc;
        char *code;
        void copy_code(std::ifstream program);
    public:
        VM(std::ifstream program);
        VM(char *code);
        void execute();
};
