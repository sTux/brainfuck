#include "Compiler.h"
#include <vector>


Compiler::Compiler(char *file): pos(0), level(0) {
    std::ifstream program(file);
    if (program.is_open()) {
        // Move to the end of the file and get the length of the file
        program.seekg(0, program.end);
        int length = program.tellg();
        // Reset back to the start to start reading
        program.seekg(0, program.beg);

        // Allocate the buffer and copy the code
        this->code = new char[length];
        program.read(this->code, length);
    }
}


bool Compiler::is_valid() {
    return this->level == 0;
}


void Compiler::compile() {
    for (char *c = this->code; *c != '\0'; c++)
        std::cout << *c;
    std::cout << std::endl;
    // Initialize previous instruction tracker with the first instruction
    char prev = *this->code;
    int repetation = 0;

    for (char *curr=this->code; *curr!='\0'; curr++) {
        // If we're seeing the same instruction, increment repetation count
        if (prev == *curr) repetation++;
        // Else process the old instruction and update previous character
        else {
            this->process_instruction(prev, repetation);
            prev = *curr;
            repetation = 1;
        }
    }
}


void Compiler::process_instruction(char instruction, int args) {
    if (instruction == '[') {
        std::cout << "Process start of loop" << std::endl;
    } else {
    }
}
