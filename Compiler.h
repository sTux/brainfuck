#include <iostream>
#include <fstream>


class Compiler {
    private:
        int pos;
        int level;
        char *code;
        void process_instruction(char instruction, int args);
    public:
        Compiler(char *file);
        bool is_valid();
        void compile();
};
